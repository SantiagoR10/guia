 $(function(){
            $("[data-toggle='tooltip']").tooltip();
            $("[data-toggle='popover']").popover();
            $('.carousel').carousel({
                interval: 4000
            });

            $('#registro').on('show.bs.modal', function (e){
                console.log('el modal registro se esta mostrando');

                $('#registroBtn').removeClass('btn-outline-success');
                $('#registroBtn').addClass('btn-info');
                $('#registroBtn').prop('disabled',true);

            });

            $('#registro').on('shown.bs.modal', function (e){
                console.log('el modal registro se mostro');
            });

            $('#registro').on('hide.bs.modal', function (e){
                console.log('el modal registro se oculta');
            });

            $('#registro').on('hidden.bs.modal', function (e){
                console.log('el modal registro se oculto');
                $('#registroBtn').prop('disabled',false);
                $('#registroBtn').removeClass('btn-info');
                $('#registroBtn').addClass('btn-outline-success');
            });
        });